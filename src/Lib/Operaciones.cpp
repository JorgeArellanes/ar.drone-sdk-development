#include "../Definiciones/DefinicionesGenerales.h"
#include "Utils.cpp"
#include "Socket.cpp"
using namespace std;

class Operaciones {
	int tamanio_cadena = 0, numero_secuencia = 1;
	int tamBuff;
	char *comando;
	struct sockaddr_in si_me, si_other,to;
	char funcionByte[];
	ArrayMultiTipo *datos_a_concatenar;
	ArrayMultiTipo dato_a_concatenar;
	Utils utilerias;

	public:
		Operaciones() {
			abrir_comunicacion();
		}
		int selecion_operacion(int operacion) {
			switch (operacion) {
			case 1:
				elevar();
				return 0;
				break;
			case 2:
				aterrizar();
				return 0;
				break;
			case 3:
				aterrizar_emergencia();
				return 0;
				break;
			case 4:
				//return centrar();
				break;
			case 5:
				return 0;
				break;
			case 6:
				return 0;
				break;
			case 7:
				return 0;
				break;
			case 10:
				estabilizar();
				return 0;
				break;
			default:
				return 0;
				break;
			}
		}

		char *elevar(){
			generar_basicos(numero_secuencia_siguiente(), VALOR_DESPEGAR, COMANDO_REF);
			enviar_datagrama();
			return get_comando();
		}

		char *aterrizar(){
			generar_basicos(numero_secuencia_siguiente(), VALOR_ATERRIZAR, COMANDO_REF);
			enviar_datagrama();
			return get_comando();
		}

		char *aterrizar_emergencia(){
			generar_basicos(numero_secuencia_siguiente(), VALOR_ATERRZAR_EMERGENCIA, COMANDO_REF);
			enviar_datagrama();
			return get_comando();
		}

		char *estabilizar() {
			generar_estabilizador(numero_secuencia_siguiente(), COMANDO_FTRIM);
			enviar_datagrama();
			return get_comando();
		}

		char *centrar(){
			std::ostringstream parametros_default;
			parametros_default << "1,0,0,0,0";
			generar_movimientosIDAA(COMANDO_PCMD, numero_secuencia_siguiente(), parametros_default.str());
			enviar_datagrama();
			return get_comando();
		}

		char *mover_izquierda(){
			int valor_movimiento = PUNTO_CERO_1_NEGATIVO;
			/*datos_a_concatenar[0].set_valor("1,",1);
			datos_a_concatenar[1].set_valor(valor_movimiento,2);
			datos_a_concatenar[2].set_valor(",0,0,0",3);
			string prametros = utilerias.concatena(datos_a_concatenar);
			cout << prametros;
			return generar_movimientosIDAA(COMANDO_PCMD, numero_secuencia_siguiente(), utilerias.concatena(datos_a_concatenar));
			*/
			std::ostringstream parametros_default;
			parametros_default << "1," << valor_movimiento << ",0,0,0";
			generar_movimientosIDAA(COMANDO_PCMD, numero_secuencia_siguiente(), parametros_default.str());
			enviar_datagrama();
			return get_comando();
		}

		char *mover_derecha(){
			int valor_movimiento = PUNTO_CERO_1_POSITIVO;
			std::ostringstream parametros_default;
			parametros_default << "1," << valor_movimiento << ",0,0,0";
			generar_movimientosIDAA(COMANDO_PCMD, numero_secuencia_siguiente(), parametros_default.str());
			enviar_datagrama();
			return get_comando();
		}

		char *mover_adelante(){
			string parametros_default = "1,0,0.1,0,0";
			return generar_movimientosIDAA(COMANDO_PCMD, numero_secuencia_siguiente(), parametros_default);
		}

		char *mover_atras(){
			string parametros_default = "1,0,-0.1,0,0";
			return generar_movimientosIDAA(COMANDO_PCMD, numero_secuencia_siguiente(), parametros_default);
		}

		char *aumenta_velocidad_vertical(){
			string parametros_default = "1,0,0,0.1,0";
			return generar_movimientosIDAA(COMANDO_PCMD, numero_secuencia_siguiente(), parametros_default);
		}

		char *disminuye_velocidad_vertical(){
			string parametros_default = "1,0,0,-0.1,0";
			return generar_movimientosIDAA(COMANDO_PCMD, numero_secuencia_siguiente(), parametros_default);
		}

		char *aumenta_velocidad_angular(){
			string parametros_default = "1,0,0,0,0.1";
			return generar_movimientosIDAA(COMANDO_PCMD, numero_secuencia_siguiente(), parametros_default);
		}

		char *disminuye_velocidad_angular(){
			string parametros_default = "1,0,0,0,-0.1";
			return generar_movimientosIDAA(COMANDO_PCMD, numero_secuencia_siguiente(), parametros_default);
		}

		//Funciones en la cuales se le puede pasar el incremento del movimiento

		char *mover_izquierda(int incremento){

		}

		char *mover_derecha(int incremento){

		}

		char *mover_adelante(int incremento){

		}

		char *mover_atras(int incremento){

		}

		char *aumenta_velocidad_vertical(int incremento){

		}

		char *disminuye_velocidad_vertical(int incremento){

		}

		char *aumenta_velocidad_angular(int incremento){

		}

		char *disminuye_velocidad_angular(int incremento){

		}

		int get_tamanio_buffer () {
			return tamanio_cadena;
		}
	private:
		char *generar_estabilizador(int numero_secuencia, string funcion) {
			std::ostringstream cadena_operacion;
			cadena_operacion << funcion << numero_secuencia << "\r";
			string cadena = cadena_operacion.str();
			set_comando(const_cast<char*>(cadena.c_str()));
			calcula_tamanio_buffer(get_comando());
			return get_comando();
		}

		char *generar_basicos(int numero_secuencia, int parametro_funcion, string funcion) {
			std::ostringstream cadena_operacion;
			cadena_operacion << funcion << numero_secuencia << "," << parametro_funcion << "\r";
			string cadena = cadena_operacion.str();
			set_comando(const_cast<char*>(cadena.c_str()));
			calcula_tamanio_buffer(get_comando());
			return get_comando();
		}

		char *generar_movimientosIDAA(string funcion, int numero_secuencia, string parametro_funcion) {
			std::ostringstream cadena_operacion;
			cadena_operacion << funcion << numero_secuencia << "," << parametro_funcion << "\r";
			string cadena = cadena_operacion.str();
			set_comando(const_cast<char*>(cadena.c_str()));
			calcula_tamanio_buffer(get_comando());
			return get_comando();
		}

		int numero_secuencia_siguiente(){
			return numero_secuencia++;
		}

		void calcula_tamanio_buffer(char *funcion){
			set_tamanio_buffer( strlen( funcion ) );
		}

		void set_tamanio_buffer(int tamanio) {
			tamanio_cadena = tamanio;
		}

		void set_comando(char *comando){
			this->comando = comando;
		}

		char *get_comando(){
			return this->comando;
		}

		void enviar_datagrama(){
			navdata_write ((int8_t*)get_comando(), get_tamanio_buffer());
		}

		void abrir_comunicacion(){
			cout << "Comunicacion estabelcida";
			navdata_open_server();
		}
};
