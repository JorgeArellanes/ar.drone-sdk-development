#include "ArrayMultiTipo.cpp"
using namespace std;
class Utils {
	std::ostringstream cadena_operacion;
public:

	string concatena(ArrayMultiTipo *valores) {
		int numero_datos = tamanio_arreglo(valores);
		ArrayMultiTipo *ordenado = arreglo_multiple_ordenado(valores);
		for(int i = 0; i<numero_datos; i++){
			switch(ordenado[i].get_tipo()) {
				case 1:
					cadena_operacion << cadena_operacion << ordenado[i].get_valor_i();
					break;
				case 2:
					cadena_operacion << cadena_operacion << ordenado[i].get_valor_s();
					break;
				case 3:
					cadena_operacion << cadena_operacion << ordenado[i].get_valor_c();
					break;
				default:
					cadena_operacion << cadena_operacion;
					break;
			}
		}
		return cadena_operacion.str();
	}

private:
	int tamanio_arreglo(ArrayMultiTipo *valores){
		return sizeof(valores);
	}

	ArrayMultiTipo *arreglo_multiple_ordenado (ArrayMultiTipo *valores){
		int numero_datos = tamanio_arreglo(valores);
		for(int i = 1; i<numero_datos; i++) {
			for(int j = 0; j<numero_datos - 1; j++) {
				if (valores[j].get_secuencia() > valores[j+1].get_secuencia()){
					ArrayMultiTipo temp = valores[j];
					valores[j] = valores[j+1];
					valores[j+1] = temp;
				}
			}
		}
		return valores;
	}

};
