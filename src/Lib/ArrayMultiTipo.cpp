#include "../Includes/IncludesGenerales.h"
using namespace std;
class ArrayMultiTipo {
	int tipo,secuencia;
	int valor_entero;
	string valor_cadena;
	char valor_caracter;
public:
	void set_valor(int valor, int secuencia){
		valor_entero = valor;
		this->secuencia = secuencia;
		this->tipo = 1;
	}

	void set_valor(string valor, int secuencia){
		valor_cadena = valor;
		this->secuencia = secuencia;
		this->tipo = 2;
	}

	void set_valor(char valor, int secuencia){
		valor_caracter = valor;
		this->secuencia = secuencia;
		this->tipo = 3;
	}

	int get_valor_i() {
		return valor_entero;
	}

	string get_valor_s() {
		return valor_cadena;
	}

	char get_valor_c() {
		return valor_caracter;
	}

	int get_secuencia(){
		return this->secuencia = secuencia;
	}

	int get_tipo(){
		return this->tipo;
	}
};
