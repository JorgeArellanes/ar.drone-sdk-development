#include "../Definiciones/DefinicionesGenerales.h";
#include <iostream>
using namespace std;
static int navdata_udp_socket  = -1;

/*
static void enviar_datagrama(char *mensaje, int tamanio){
	navdata_write((int8_t*)mensaje, tamanio);
}*/


static void navdata_write(int8_t *buffer, int32_t len) {
	cout<< "Mensaje >> " << buffer;
	cout<< "Tamaño >> " << len << "\n";
	struct sockaddr_in to;
	int32_t flags;
	struct sockaddr_in navdata_udp_addr;
	memset( (char*)&navdata_udp_addr, 0, sizeof(navdata_udp_addr) );
	navdata_udp_addr.sin_family      = AF_INET;
	navdata_udp_addr.sin_addr.s_addr = INADDR_ANY;
	navdata_udp_addr.sin_port        = htons(NAVDATA_PORT + 100);
	//open socket
	navdata_udp_socket = socket( AF_INET, SOCK_DGRAM, 0 );
	if( navdata_udp_socket >= 0 ) {
		int res;
		memset( (char*)&to, 0, sizeof(to) );
		to.sin_family       = AF_INET;
		to.sin_addr.s_addr  = inet_addr(WIFI_ARDRONE_IP); // BROADCAST address for subnet 192.168.1.xxx
		to.sin_port         = htons( NAVDATA_PORT );
		res = sendto( navdata_udp_socket, (char*)buffer, len, 0, (struct sockaddr*)&to, sizeof(to) );
	}
}

static void navdata_open_server (void) {
	int32_t one = 1;
    navdata_write ((int8_t*)&one, sizeof( one ));
}
