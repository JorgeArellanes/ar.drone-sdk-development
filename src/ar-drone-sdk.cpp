//============================================================================
// Name        : TestDrone.cpp
// Author      : Jorge Carreño Arellanes
// Version     :
// Copyright   : Derechos Reservados, bajo licencia GPLv2
// Description : Hello World in C++, Ansi-style
//============================================================================
#include "Lib/Operaciones.cpp"
using namespace std;

int main(int argc, char* time[]) {
	string menu = "\n\n\n\n\n\nElige funcion : \n\t0.-Terminar\n\t1.-Despegar\n\t2.-Aterrizar\n\t3.-Emergencia\n\t4.-Derecha\n\t5.-Izquierda\n\t6.-Centrar\n\n";
	Operaciones operaciones;
	int operacion;
    cout<< menu;
    cin >> operacion;
    while( operacion != 0 ) {

    	if(operacion > 0 && operacion < 4 ) {
    		operaciones.selecion_operacion(operacion);
    	}

    	if(operacion == 4) {
    		operaciones.mover_derecha();
			operaciones.estabilizar();
		}

    	if(operacion == 5) {
			operaciones.mover_izquierda();
			operaciones.estabilizar();
		}

    	if(operacion == 6) {
			operaciones.centrar();
			operaciones.estabilizar();
		}
    	operaciones.selecion_operacion(10);

    	cout<< menu;
    	cin >> operacion;
    }
}
